package ca.uqam.mgl.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ca.uqam.mgl.entity.CarteMagasinPK;
import ca.uqam.mgl.entity.Transaction;

@Repository
public interface ITransactionDao extends JpaRepository<Transaction, CarteMagasinPK> {

}
