package ca.uqam.mgl.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ca.uqam.mgl.entity.Magasin;

@Repository
public interface IMagasinDao extends JpaRepository<Magasin, Long> {

}
