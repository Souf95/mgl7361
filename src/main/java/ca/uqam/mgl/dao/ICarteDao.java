package ca.uqam.mgl.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ca.uqam.mgl.entity.Carte;

@Repository
public interface ICarteDao extends JpaRepository<Carte, Long> {

}
