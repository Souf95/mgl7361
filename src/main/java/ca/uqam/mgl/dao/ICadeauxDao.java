package ca.uqam.mgl.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ca.uqam.mgl.entity.Cadeaux;

@Repository
public interface ICadeauxDao extends JpaRepository<Cadeaux, Long> {

}
