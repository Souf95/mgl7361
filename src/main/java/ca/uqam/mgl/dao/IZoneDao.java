package ca.uqam.mgl.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ca.uqam.mgl.entity.Zone;

@Repository
public interface IZoneDao extends JpaRepository<Zone, Long> {

}
