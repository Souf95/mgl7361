package ca.uqam.mgl.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import ca.uqam.mgl.transversal.entity.AbstractEntity;

@Entity
@Table(name = "MAGASIN")
public class Magasin extends AbstractEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1076973299046816245L;

	@Column(name = "LABELLE")
	private String labelle;

	@Column(name = "ADRESSE")
	private String adresse;

	@OneToMany(mappedBy = "magasin", cascade = { CascadeType.MERGE, CascadeType.PERSIST }, fetch = FetchType.LAZY)
	private List<Cadeaux> listCadeaux;

	@OneToMany(mappedBy = "magasin", cascade = { CascadeType.MERGE, CascadeType.PERSIST }, fetch = FetchType.LAZY)
	private List<Transaction> listTransactions;

	@ManyToOne
	@JoinColumn(name = "FK_ZONE")
	private Zone zone;

	public String getLabelle() {
		return labelle;
	}

	public void setLabelle(String labelle) {
		this.labelle = labelle;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public List<Cadeaux> getListCadeaux() {
		return listCadeaux;
	}

	public void setListCadeaux(List<Cadeaux> listCadeaux) {
		this.listCadeaux = listCadeaux;
	}

	public List<Transaction> getListTransactions() {
		return listTransactions;
	}

	public void setListTransactions(List<Transaction> listTransactions) {
		this.listTransactions = listTransactions;
	}

	public Zone getZone() {
		return zone;
	}

	public void setZone(Zone zone) {
		this.zone = zone;
	}

	public Magasin() {
		super();
	}

}
