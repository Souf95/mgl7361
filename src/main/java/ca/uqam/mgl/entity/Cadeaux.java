package ca.uqam.mgl.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import ca.uqam.mgl.transversal.entity.AbstractEntity;

@Entity
@Table(name = "CADEAUX")
public class Cadeaux extends AbstractEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3746317193224413931L;

	@Column(name = "LABELLE")
	private String labelle;

	@Column(name = "DESCRIPTON")
	private String description;

	@Column(name = "NOMBRE_POINT")
	private Integer nbrPoint;

	@ManyToOne
	@JoinColumn(name = "FK_ZONE")
	private Zone zone;

	@ManyToOne
	@JoinColumn(name = "FK_MAGASIN")
	private Magasin magasin;

	public String getLabelle() {
		return labelle;
	}

	public void setLabelle(String labelle) {
		this.labelle = labelle;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getNbrPoint() {
		return nbrPoint;
	}

	public void setNbrPoint(Integer nbrPoint) {
		this.nbrPoint = nbrPoint;
	}

	public Zone getZone() {
		return zone;
	}

	public void setZone(Zone zone) {
		this.zone = zone;
	}

	public Magasin getMagasin() {
		return magasin;
	}

	public void setMagasin(Magasin magasin) {
		this.magasin = magasin;
	}

	public Cadeaux() {
		super();
	}
}
