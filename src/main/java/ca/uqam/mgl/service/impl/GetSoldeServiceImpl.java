package ca.uqam.mgl.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ca.uqam.mgl.dao.ICarteDao;
import ca.uqam.mgl.dto.CarteDTO;
import ca.uqam.mgl.entity.Carte;
import ca.uqam.mgl.service.IGetSoldeService;
import ca.uqam.mgl.transversal.dto.IBeanDTO;
import ca.uqam.mgl.transversal.service.AbstractTemplateCustomService;

@Service("getSoldeService")
public class GetSoldeServiceImpl extends AbstractTemplateCustomService implements IGetSoldeService {

	@Autowired
	private ICarteDao carteDao;

	@Override
	public IBeanDTO doProcess(IBeanDTO dto) {
		CarteDTO idDTO = (CarteDTO) dto;
		Carte carte = carteDao.getById(idDTO.getId());
		CarteDTO cartedto = new CarteDTO(carte.getDateCreation(), carte.getSolde(), carte.getDateExpiration(),
				carte.getIsBlocked());
		return cartedto;
	}

}
