package ca.uqam.mgl.ws;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ca.uqam.mgl.dto.CarteDTO;
import ca.uqam.mgl.service.IGetSoldeService;

@RestController
@RequestMapping("api/v1/carte")
@CrossOrigin("*")
public class FacadeCarteController {

	@Autowired
	private IGetSoldeService getSoldeService;
	
	@GetMapping(path = "/getSolde")
	public Integer uploadHistoriqueSegmentationImage(@RequestBody CarteDTO idDTO) {
		CarteDTO dto = (CarteDTO) getSoldeService.process(idDTO);
		return dto.getSolde();
	}
}
