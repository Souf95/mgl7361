package ca.uqam.mgl.transversal.service;

import ca.uqam.mgl.entity.Client;

public interface IDummyService {

	Client getDummyUserById(Long id);
}
