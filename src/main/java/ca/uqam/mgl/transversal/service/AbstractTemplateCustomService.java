package ca.uqam.mgl.transversal.service;

import ca.uqam.mgl.transversal.dto.IBeanDTO;

public abstract class AbstractTemplateCustomService implements ITemplateCustomService{

	
	@Override
	public IBeanDTO process(IBeanDTO dto) {
		// Step 1 : deleguer le traitement métier au service spécifique	
		
		return doProcess(dto);
	}

	/**
	 * méthode abstract , l'implementation se fait dans le service métier spécifique
	 * @param Generic
	 * @return Generic
	 */
	public abstract IBeanDTO doProcess(IBeanDTO dto);
}
